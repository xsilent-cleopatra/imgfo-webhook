# ImgFo Webhook Script
This script will allow https://imgfo.com to upload the content to your own webserver storage.

## Usage
1. Just upload the `imgfo-webhook.php` to your public_html directory.
2. Go to [ImgFo Website](https://imgfo.com), then input the field Webhook URL.  
  ex: `https://yourdomain.com/imgfo-webhook.php`
3. Done, Now you are able to **Upload via Webhook**.


## Contributions
- Currently I've created for PHP because very simple to deploy (only single file) into free hosting.
- Any contribution are welcome.
